<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
        public function handle($request, Closure $next)
    {
        
        if(auth()->check()){
        if (auth()->user()->isAdmin == 1) {
                        

            return $next($request);
        }
        elseif(auth()->user()->isAdmin == 2 ){
            return redirect('subadmin');

        }
           elseif(auth()->user()->isAdmin == 3){
            return redirect('home');

        }
        else
        {
        return redirect('/login');
        }
}
else{

  return redirect('/login');
}
    
}
}