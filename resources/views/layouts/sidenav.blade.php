<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
      <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{auth()->user()->name}}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->


    
    <ul class="sidebar-menu">
      {{-- ==================================================================================
        ==============================ADMIN MENU END================================
        ================================================================================== --}}
        @if(auth()->user()->isAdmin == 1) 
       <li class="header">MAIN NAVIGATION</li>
        
          
        <li class="active"> {{-- treeview --}}
          <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Admin Dashboard (Admin)</span> </i>
          </a>
          </li>
        <li class=""> {{-- treeview --}}
          <a href="/add-subadmin">
            <i class="fa fa-users"></i> <span>Add SubAdmin</span> </i>
          </a>
          </li>



      @endif
        {{-- ========================== --}}
        {{-- ========END ADMIN========== --}}
        {{-- ========================== --}}
       {{-- ==================================================================================
        ==============================SUB ADMIN MENU END================================
        ================================================================================== --}}
        
        @if(auth()->user()->isAdmin == 2) 
       <li class="header">MAIN NAVIGATION</li>
       
        <li class="active"> {{-- treeview --}}
          <a href="/subadmin">
            <i class="fa fa-dashboard"></i> <span>Admin Dashboard (subAdmin)</span> </i>
          </a>
          </li>
    
    
 
       @endif
        {{-- ========================== --}}
        {{-- ========END SUB ADMIN========== --}}
        {{-- ========================== --}}

        
        {{-- ==================================================================================
          ==============================USER MENU==============================================
          ================================================================================== --}}
          @if(auth()->user()->isAdmin == 3)
            <li class="header">MAIN NAVIGATION</li> 
            
          <li class="active"> {{-- treeview --}}
            <a href="/user">
              <i class="fa fa-dashboard"></i> <span>Admin Dashboard (user)</span> </i>
            </a>
            </li>
  
  
        @endif
        {{-- ========================== --}}
        {{-- ========END USER========== --}}
        {{-- ========================== --}}
         

















      
      {{-- ===================================================================  --}}
      {{-- ==============================Teacher===========================  --}}
      {{-- <li class="treeview">
        <a href="#">
          <i class="fa fa-male"></i>
          <span>Teacher</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href=""><i class="fa fa-circle-o"></i> All Teacher</a></li>
          <li><a href=""><i class="fa fa-circle-o"></i> Teacher Details</a></li>
          <li><a href=""><i class="fa fa-circle-o"></i> Add Teacher</a></li>
          <li><a href=""><i class="fa fa-circle-o"></i> Payment</a></li>
        </ul>
      </li> --}}
      {{-- ===================================================================  --}}
      
        {{-- ===================================school profile=========================  --}}
        {{-- <li>
          <a href="/school/profile">
            <i class="fa fa-circle-o"></i> <span>School Profile</span>
          </a>
        </li> --}}
        {{-- ================================================================== --}}
        {{-- ============================Library=============================  --}}
        {{-- <li class="treeview">
          <a href="#">
            <i class="fa fa-folder-open"></i>
            <span>Library</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i> All Book</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Add New Book</a></li>
          </ul>
        </li> --}}
        {{-- ===================================================================  --}}
          
      
        {{-- ===================================Class Routine=========================  --}}
        {{-- <li>
          <a href="">
            <i class="fa fa-calendar"></i> <span>Class Routine</span>
            <!--<small class="label pull-right bg-yellow">12</small>-->
          </a>
        </li> --}}

        {{-- ================================================================== --}}
        
        

        {{-- ===================================Transport=========================  --}}
        {{-- <li>
          <a href="">
            <i class="fa fa-bus"></i> <span>Transport</span>
            <!--<small class="label pull-right bg-yellow">12</small>-->
          </a>
        </li> --}}

        {{-- ================================================================== --}}
      
        {{-- ===================================Message=========================  --}}
        {{-- <li>
          <a href="">
            <i class="fa fa-envelope"></i> <span>Message</span>
            <!--<small class="label pull-right bg-yellow">12</small>-->
          </a>
        </li> --}}
        {{-- ================================================================== --}}
      
        {{-- ===================================Roles=========================  --}}
        {{-- <li>
          <a href="">
            <i class="fa fa-users"></i> <span>Roles</span>
            <!--<small class="label pull-right bg-yellow">12</small>-->
          </a>
        </li> --}}

        {{-- ================================================================== --}}

      {{-- <li class="treeview">
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>UI Elements</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
          <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
          <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
          <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
          <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
          <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Forms</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
          <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
          <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-table"></i> <span>Tables</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
          <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
        </ul>
      </li>
      <li>
        <a href="pages/calendar.html">
          <i class="fa fa-calendar"></i> <span>Calendar</span>
          <small class="label pull-right bg-red">3</small>
        </a>
      </li>
      <li>
        <a href="pages/mailbox/mailbox.html">
          <i class="fa fa-envelope"></i> <span>Mailbox</span>
          <!--<small class="label pull-right bg-yellow">12</small>-->
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i> <span>Examples</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
          <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
          <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
          <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
          <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
          <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
          <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
          <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Multilevel</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          <li>
            <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
        </ul>
      </li>
      <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
      <li class="header">LABELS</li>
      <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> --}}
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>